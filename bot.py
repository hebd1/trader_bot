import websocket, json, pprint, talib, numpy
import config
from coinbase.wallet.client import Client
# from coinbase.wallet.client import Client
# uses RSI indicator, close of kline candlestick to sell when overbought, buy when oversold
# increase candlestick time frame in the future
# kline contains open, high, low, and closed data (x: true - final value for the candlestick, kline closed)
SOCKET = "wss://stream.binance.com:9443/ws/ethusdt@kline_1m"
RSI_PERIOD = 14
RSI_OVERBOUGHT = 70
RSI_OVERSOLD = 30
TRADE_SYMBOL = 'ETHUSD'
TRADE_QUANTITY = 0.00903504

closes = []
in_position = False
client = Client(config.API_KEY, config.API_SECRET)

def print_transact(response):
    json_msg = json.loads(response)
    print(json_msg['transfer'])
    # # '6H7GYLXZ'
    # print(response['transfer'][['btc']['amount']])
    # # '1.00000000'
    # print(response['transfer']['total']['amount'])
    # # '$17.95'
    # print(response['transfer']['payout_date'])
    # # '2013-02-01T18:00:00-08:00' (ISO 8601 format)

def buy():
    try:
        print('sending buy order...')
        # print_transact(coinbase.buy(TRADE_QUANTITY))
    except Exception as e:
        print('Order failed')
        return False
    return True

def sell():
    try:
        print('sending sell order...')
        # print_transact(coinbase.sell(TRADE_QUANTITY))
    except Exception as e:
        print('Order failed')
        return False
    return True

def on_open(ws):
    print('connection established')

def on_close(ws):
    print('conneciton closed')

def on_message(ws, msg):
    global closes, in_position
    print('-- received message: ')
    json_msg = json.loads(msg)
    pprint.pprint(json_msg)

    candle = json_msg['k']
    candle_is_closed = candle['x']
    close = candle['c']
    if candle_is_closed:
        print("--- candle closed at {}".format(close))
        closes.append(float(close))
        if len(closes) > RSI_PERIOD:
            np_closes = numpy.array(closes)
            rsi = talib.RSI(np_closes, RSI_PERIOD)
            print("all rsi's calculated so far")
            print(rsi)
            last_rsi = rsi[-1] # last RSI value
            print("current RSI is {}".format(last_rsi))

            if last_rsi > RSI_OVERBOUGHT:
                if in_position:
                    print("overbought! sell sell sell!")
                    order_succeeded = sell()
                    if order_succeeded:
                        in_position = False
                else:
                    print("We don't own anything, nothing to do.")
            if (last_rsi < RSI_OVERSOLD):
                if in_position:
                    print("oversold, but you already own it, nothing to do.")
                else:
                    print ("oversold! buy buy buy!")
                    order_succeeded = buy()
                    if order_succeeded:
                        in_position = True
    print("closes: ")
    print (closes)

user = client.get_current_user()
user_as_json_string = json.dumps(user)
ws = websocket.WebSocketApp(SOCKET, on_open=on_open, on_close=on_close, on_message=on_message)
ws.run_forever()